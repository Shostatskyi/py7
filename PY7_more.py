import pickle

# Practicing pickle with recipes

cook_book = {
  'Omlette': [
    {'ingridient': 'eggs', 'quantity': 2, 'unit': 'pcs'},
    {'ingridient': 'tomatoes', 'quantity': 100, 'unit': 'g'}
  ],
  'Steak': [
    {'ingridient': 'beef', 'quantity': 300, 'unit': 'gr'},
    {'ingridient': 'spice', 'quantity': 5, 'unit': 'gr'},
    {'ingridient': 'butter', 'quantity': 100, 'unit': 'mg'}
  ],
  'Salad': [
    {'ingridient': 'tomatoes', 'quantity': 100, 'unit': 'g'},
    {'ingridient': 'cucumbers', 'quantity': 100, 'unit': 'g'},
    {'ingridient': 'butter', 'quantity': 100, 'unit': 'mg'},
    {'ingridient': 'onion', 'quantity': 1, 'unit': 'pcs'}
  ]
}

with open('recipes2.txt', 'wb') as recipes:
    pickle.dump(cook_book, recipes)

with open('recipes2.txt', 'rb') as r:
    [(print('\n', nam), [print(*x.values()) for x in ing]) for nam, ing in pickle.load(r).items()]
