import sys

# Open and read recipes from file
with open('recipes.txt') as recipes:
    print(recipes.read())
print()

# In Unicode codes
with open('recipes.txt', 'rb') as recipes:
    [print(ch, ' ', end='') for ch in recipes.read()]
print()

# Question 1: Is it right that when reading file with mode "rb" it takes less memory?

# a) read as text
print("\nSize of char when reading with mode 'r'\n")
with open('recipes.txt', 'r') as recipes:
    [print(sys.getsizeof(ch), ' ', end='') for ch in recipes.read()]
print()

# b) read as binary
print("\nSize of char when reading with mode 'rb'\n")
with open('recipes.txt', 'rb') as recipes:
    [print(sys.getsizeof(ch), ' ', end='') for ch in recipes.read()]
print()

# Questions 2:
# In Learning Python, 5th Edition, on p.287 given the following example:
# >>> data = open('data.bin', 'rb').read()
# >>> data
# b'\x00\x00\x00\x07spam\x00\x08'
# >>> data[4:8]
# b'spam'
# >>> data[4:8][0]
# However, in my case it prints as string, and I want to print as bytes (i.e. wihout deconding from file)
print("\nPrint the whole file in 'rb'\n")
with open('recipes.txt', 'rb') as recipes:
    print(recipes.read()) # prints as string with b'...' and ignores \n
print()
# Am I doing something wrong or don't understand? I'd like something like b'\x00\x00\x00 or this:
print("\nPrint each char in binary\n") # is it right that it's Unicode number in binary?
with open('recipes.txt', 'rb') as recipes:
    [ print(bin(ch), end='') for ch in recipes.read()]
print()

# Question 3: Is it right that recipes.read() in list comprehension reads the whole file each interation
# and it is therefore better to read it once and store it in a variable?
